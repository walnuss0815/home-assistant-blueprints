blueprint:
  name: Advanced Motion-activated Light
  description: Turn on a light when motion is detected depending on night and sleep modes.
  domain: automation
  input:
    ## Motion sensor
    motion_entity:
      name: Motion Sensor
      description: Could be a single motion sensor or a group of motion sensors
      selector:
        entity:

    ## Light target
    light_target:
      name: Light
      selector:
        entity:
          domain: light

    ## Brightness
    night_light_brightness:
      name: Night light brightness
      default: 200
      selector:
        number:
          min: 0
          max: 255
    sleep_light_brightness:
      name: Sleep light brightness
      default: 25
      selector:
        number:
          min: 0
          max: 255

    ## Color temp
    night_light_color_temp:
      name: Night light color temperature
      default: 300
      selector:
        number:
          min: 153
          max: 500
    sleep_light_color_temp:
      name: Sleep light color temperature
      default: 300
      selector:
        number:
          min: 153
          max: 500

    ## Timeout
    night_light_timeout:
      name: Night light time
      description: Time to leave the light on after last motion is detected.
      default: 600
      selector:
        number:
          min: 0
          max: 3600
          unit_of_measurement: seconds
    sleep_light_timeout:
      name: Sleep light time
      description: Time to leave the light on after last motion is detected.
      default: 120
      selector:
        number:
          min: 0
          max: 3600
          unit_of_measurement: seconds

    ## Timeout
    min_timeout:
      name: Minimum Timout
      description: Time to leave the light on minimum
      default: 60
      selector:
        number:
          min: 0
          max: 3600
          unit_of_measurement: seconds

    ## Mode entities
    night_mode_entity:
      name: Night mode entity
      description: Boolean entity which indicates its night time or not
      selector:
        entity:
    sleep_mode_entity:
      name: Sleep mode entity
      description: Boolean entity which indicates its night time or not
      selector:
        entity:

mode: restart

trigger:
  - platform: state
    entity_id: !input motion_entity
    to: 'on'
  - platform: state
    entity_id: !input motion_entity
    to: 'off'
    for:
      seconds: !input night_light_timeout
  - platform: state
    entity_id: !input motion_entity
    to: 'off'
    for:
      seconds: !input sleep_light_timeout
  - platform: state
    entity_id: !input light_target
    to: 'on'
    for:
      seconds: !input night_light_timeout
  - platform: state
    entity_id: !input light_target
    for:
      seconds: !input sleep_light_timeout
    to: 'on'
  - platform: state
    entity_id: !input light_target
    for:
      seconds: !input min_timeout
    to: 'on'
  - platform: state
    entity_id: !input night_mode_entity
    to: 'on'
  - platform: state
    entity_id: !input sleep_mode_entity
    to: 'on'
    ## Workaround for failed/untriggered triggers
  - platform: time_pattern
    minutes: "/30"

condition: []

action:
  - choose:
      - conditions:
          - condition: state
            entity_id: !input motion_entity
            state: 'on'
          - condition: state
            entity_id: !input light_target
            state: 'off'
        sequence:
          - choose:
              - conditions:
                  - condition: state
                    entity_id: !input sleep_mode_entity
                    state: 'on'
                  - condition: state
                    entity_id: !input night_mode_entity
                    state: 'on'
                sequence:
                  - service: light.turn_on
                    target:
                      entity_id: !input light_target
                    data:
                      brightness: !input sleep_light_brightness
                      color_temp: !input sleep_light_color_temp
              - conditions:
                  - condition: state
                    entity_id: !input sleep_mode_entity
                    state: 'off'
                  - condition: state
                    entity_id: !input night_mode_entity
                    state: 'on'
                sequence:
                  - service: light.turn_on
                    target:
                      entity_id: !input light_target
                    data:
                      brightness: !input night_light_brightness
                      color_temp: !input night_light_color_temp
            default: []
      - conditions:
          - condition: state
            entity_id: !input light_target
            state: 'on'
            for:
              seconds: !input min_timeout
          - condition: or
            conditions:
              - condition: and
                conditions:
                  - condition: state
                    entity_id: !input sleep_mode_entity
                    state: 'on'
                  - condition: state
                    entity_id: !input motion_entity
                    state: 'off'
                    for:
                      seconds: !input sleep_light_timeout
                  - condition: state
                    entity_id: !input light_target
                    state: 'on'
                    for:
                      seconds: !input sleep_light_timeout
              - condition: and
                conditions:
                  - condition: state
                    entity_id: !input motion_entity
                    state: 'off'
                    for:
                      seconds: !input night_light_timeout
                  - condition: state
                    entity_id: !input light_target
                    state: 'on'
                    for:
                      seconds: !input night_light_timeout
        sequence:
          - service: light.turn_off
            target:
              entity_id: !input light_target
            data:
              transition: 5
    default: []
